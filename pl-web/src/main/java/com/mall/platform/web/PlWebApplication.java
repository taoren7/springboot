package com.mall.platform.web;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.mall.platform.web.bean.Product;
import com.mall.platform.web.model.RestResponse;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;
import java.util.Date;
import java.util.HashMap;

//@SpringBootApplication
public class PlWebApplication {

    public static void main(String[] args) {
        //ConfigurableApplicationContext context = SpringApplication.run(PlWebApplication.class, args);
        RestTemplate rt = new RestTemplate();
        //testGet(rt);
        //testPost(rt);
        String url = "http://127.0.0.1:8080/product/add";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
//        MultiValueMap<String, String> map= new LinkedMultiValueMap<>();
//        map.add("pname", "lily");
//        map.add("type", "dddd");
//        map.add("price", 100+"");
//        map.add("createTime", "2019-01-01 12:12:12");
        Product product = new Product();
        product.setPname("lilyggg");
        product.setType("ttttggg");
        product.setPrice(200);
        product.setCreateTime(new Date());
        HttpEntity<Product> request = new HttpEntity<>(product, headers);
        String response = rt.postForObject(url, request, String.class);
        System.out.println(response);
//        ResponseEntity<String> entity = (ResponseEntity<String>) response;
//        System.out.println(entity);
    }

    private static void testPost(RestTemplate rt) {
        rt.getMessageConverters().add(new StringHttpMessageConverter(Charset.forName("utf-8")));
        String url = "http://127.0.0.1:8080/product/add";
        MultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
        map.add("pname", "lily");
        map.add("type", "dddd");
        map.add("price", 100);
        map.add("createTime", "2019-01-01 12:12:12");
        String response = rt.postForObject(url, map, String.class);
        System.out.println(response);
    }

    private static void testGet(RestTemplate rt) {
        String url = "http://127.0.0.1:8080/product/getById?id={id}";
        HashMap<String, String> map = new HashMap<>();
        map.put("id", "78");
        RestResponse response = rt.getForObject(url, RestResponse.class, map);
        String body = rt.getForObject(url, String.class, 78);
        System.out.println(body);
        RestResponse restResponse = JSON.parseObject(body, RestResponse.class);
        String dataStr = JSON.toJSONString(restResponse.getData());
        Product result = JSON.parseObject(dataStr, Product.class);
        System.out.println(result);
        JSONObject jsonObject = (JSONObject) restResponse.getData();
        Product product = jsonObject.toJavaObject(Product.class);
        System.out.println(product);
    }

}
