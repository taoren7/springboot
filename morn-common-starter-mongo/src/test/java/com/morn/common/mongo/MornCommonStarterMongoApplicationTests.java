package com.morn.common.mongo;

import com.alibaba.fastjson.JSON;
import com.morn.common.mongo.dao.PersonMongoDao;
import com.morn.common.mongo.model.Person;
import com.morn.common.mongo.model.page.PageListVO;
import com.morn.common.mongo.model.page.QueryPage;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MornCommonStarterMongoApplicationTests {

    @Autowired
    private PersonMongoDao personMongoDao;

    @Test
    public void save() {
        Person p = new Person(5,"赵六",18);
        Person person = personMongoDao.save(p);
        System.out.println(person);

    }


    @Test
    public void findPage() {
        Person p = new Person();
        QueryPage page = new QueryPage(2,2);
        PageListVO<Person> pageListVO = personMongoDao.findPage(page, p);
        System.out.println(JSON.toJSONString(pageListVO));
    }
    @Test
    public void find() {
        Person p = new Person();
        List<Person> persons = personMongoDao.find(p);
        System.out.println(persons);
    }
    @Test
    public void findOne() {
        Person p = new Person();
        Person person = personMongoDao.findOne(p);
        System.out.println(person);
    }

    @Test
    public void findById() {
        Person person = personMongoDao.findById(1);
        System.out.println(person);
    }



}
