package com.morn.common.mongo.dao;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.Page;
import com.morn.common.mongo.model.page.PageListVO;
import com.morn.common.mongo.model.page.QueryPage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.BasicQuery;
import org.springframework.data.mongodb.core.query.BasicUpdate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.util.List;

public abstract class AbstractMongoDAO<T, ID> implements BaseMongoDAO<T, ID> {
	
	/**
	 * 实体model的class
	 * @return
	 */
	protected abstract Class<T> getModelClass();

	protected abstract String getTable();
	
	@Autowired
	private MongoTemplate mongoTemplate;
	
	protected MongoTemplate getDao() {
		return mongoTemplate;
	}

	@Override
	public List<T> find(T t) {
		String json = JSON.toJSONString(t);
		Query query = new BasicQuery(json);
		return this.find(query, getTable());
	}

	@Override
	public T findOne(T t) {
		String json = JSON.toJSONString(t);
		Query query = new BasicQuery(json);
		return this.getDao().findOne(query, this.getModelClass(), getTable());
	}

	@Override
	public void update(T q, T t) {
		String queryJson = JSON.toJSONString(q);
		Query query = new BasicQuery(queryJson);
		String updateJson = JSON.toJSONString(t);
		Update update = new BasicUpdate(updateJson);
		// this.getDao().findAndModify(query, update, this.getModelClass());
		this.getDao().upsert(query, update, this.getModelClass(), getTable());
	}

	@Override
	public void remove(T q) {
		String queryJson = JSON.toJSONString(q);
		Query query = new BasicQuery(queryJson);
		// this.getDao().findAndModify(query, update, this.getModelClass());
		this.getDao().remove(query, this.getModelClass(), getTable());
	}

	@Override
	public T save(T entity) {
		T t = this.getDao().insert(entity, getTable());
		return t;
	}

	@Override
	public T findById(ID id) {
		return this.getDao().findById(id, this.getModelClass(), getTable());
	}

	@Override
	public PageListVO<T> findPage(QueryPage pageQuery, T t) {
		PageListVO<T> result = new PageListVO<>();
		String json = JSON.toJSONString(t);
		Query query = new BasicQuery(json);
		long count = this.count(query , getTable());
		int pageNumber = pageQuery.getPageNum();
		int pageSize = pageQuery.getPageSize();
		query.skip((pageNumber - 1) * pageSize).limit(pageSize);
		List<T> rows = this.find(query, getTable());
		Page<T> list = new Page<>(pageQuery.getPageNum(), pageQuery.getPageSize());
		list.setTotal(count);
		if (null != rows && rows.size() > 0)
			list.addAll(rows);
		result.setList(list);
		return result;
	}

	@Override
	public long count(T t) {
		String json = JSON.toJSONString(t);
		Query query = new BasicQuery(json);
		return this.count(query, getTable());
	}

	private long count(Query query, String table) {
		return this.getDao().count(query, this.getModelClass(), table);
	}

	private List<T> find(Query query , String table) {
		return this.getDao().find(query, this.getModelClass(), table);
	}
}
