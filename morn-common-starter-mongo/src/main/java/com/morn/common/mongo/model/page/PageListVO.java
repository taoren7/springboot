package com.morn.common.mongo.model.page;

import java.io.Serializable;
import java.util.List;

public class PageListVO<T> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2381828676078873810L;

	private List<T> list;

	private PageVO pageVO;

	public PageListVO() {

	}

	public PageListVO(List<T> list) {
		this.pageVO = new PageVO(list);
		this.list = list;
	}

	public List<T> getList() {
		return list;
	}

	public void setList(List<T> list) {
		if(this.pageVO == null){
			this.pageVO = new PageVO(list);
		}
		this.list = list;
	}

	public PageVO getPageVO() {
		return pageVO;
	}

	public void setPageVO(PageVO pageVO) {
		this.pageVO = pageVO;
	}

}
