package com.morn.common.mongo.dao;

import com.morn.common.mongo.model.page.PageListVO;
import com.morn.common.mongo.model.page.QueryPage;

import java.util.List;

public interface BaseMongoDAO<T, ID> {
	/**
	 * 通过条件查询实体(集合)
	 * 
	 * @param query
	 */
	public List<T> find(T query);

	/**
	 * 通过一定的条件查询一个实体
	 * 
	 * @param query
	 * @return
	 */
	public T findOne(T query);

	/**
	 * 通过条件查询更新数据
	 * 
	 * @param query
	 * @param update
	 * @return
	 */
	public void update(T query, T update);

	public void remove(T query);

	/**
	 * 保存一个对象到mongodb
	 * 
	 * @param entity
	 * @return
	 */
	public T save(T entity);

	/**
	 * 通过ID获取记录
	 * 
	 * @param id
	 * @return
	 */
	public T findById(ID id);

	/**
	 * 分页查询
	 * 
	 * @param page
	 * @param query
	 * @return
	 */
	public PageListVO<T> findPage(QueryPage page, T query);

	/**
	 * 求数据总和
	 * 
	 * @param query
	 * @return
	 */
	public long count(T query);
}
