package com.morn.common.mongo.dao;

import com.morn.common.mongo.model.Person;
import org.springframework.stereotype.Component;

/**
 * @Author: 陶韧
 * @Date: Created in 2019/8/7 15:38
 * @Description:
 */
@Component
public class PersonMongoDao extends AbstractMongoDAO<Person, Integer> {
    public static final String PERSON = "person";
    @Override
    protected Class<Person> getModelClass() {
        return Person.class;
    }

    @Override
    protected String getTable() {
        return PERSON;
    }
}
