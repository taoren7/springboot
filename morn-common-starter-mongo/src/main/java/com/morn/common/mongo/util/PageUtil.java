package com.morn.common.mongo.util;

import com.github.pagehelper.PageHelper;
import com.morn.common.mongo.model.page.PageListVO;
import com.morn.common.mongo.model.page.PageVO;
import com.morn.common.mongo.model.page.QueryPage;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * @description: 分页公共代码
 * @author: tyh
 * @date: 2019/04/07
 */
public class PageUtil {

     public static void initPage(QueryPage page) {
       /* if (null == page) {
            page = new QueryPage();
            page.setFlag(false);
        }*/
        if(null == page.getFlag()){
            page.setFlag(false);
        }

        PageHelper.startPage(page.getPageNum(), page.getPageSize(), page.getFlag());
        if (!StringUtils.isEmpty(page.getOrderBy())) {
            PageHelper.orderBy(page.getOrderBy());
        }
    }

    /**
     * 获取手动分页后的数据
     * @param page 分页参数对象
     * @param sortedList 待分页的数据
     * @param <T>
     * @return 分页后的数据
     */
    public static <T> PageListVO<T> getPageListVO(QueryPage page, List<T> sortedList) {
        PageListVO<T>  pageListVO = new PageListVO<>();

        //PageUtil.initPage(page);
        int pageNum = page.getPageNum(); // 当前页码
        int pageSize = page.getPageSize(); // 每页条数
        int totalCount = sortedList.size(); // 总条数

        /*****************设置分页参数********************/
        int startIndex = (page.getPageNum()-1) * page.getPageSize(); // list 开始索引
        int endIndex = startIndex + page.getPageSize(); // list 结束索引

        PageVO pageVO = new PageVO(); // 设置当前页，总长度，开始记录数，结束记录数，是否有下一页，是否有上一页，是否第一页，是否最后一页
        pageVO.setCurrPage(pageNum); // 当前页
        pageVO.setPageSize(pageSize); // 每页记录数
        pageVO.setTotal(sortedList.size()); // 总记录数

        pageVO.setStartRow(startIndex); // 开始记录索引

        if(startIndex >= totalCount ){ // 不可能>totalCount除非参数错误

        }else if(startIndex < totalCount && endIndex >= totalCount){
            sortedList = sortedList.subList(startIndex,totalCount);
        }else if(startIndex < totalCount && endIndex < totalCount){
            sortedList = sortedList.subList(startIndex,endIndex);
        }

        pageVO.setEndRow(startIndex+sortedList.size()); // 结束记录索引
        pageVO.setSize(sortedList.size()); // 当前页实际记录数

        pageVO.setPages(totalCount % pageSize== 0?totalCount / pageSize:totalCount / pageSize + 1); // 总页数
        pageVO.setFirstPage(1); // 首页，第一页
        pageVO.setPrePage(pageNum>1?pageNum-1:0);// 上一页,0代表无
        pageVO.setNextPage(pageVO.getPages()>pageNum?pageNum+1:0);// 下一页,0代表无
        pageVO.setLastPage(pageVO.getPages());// 最后一页
        pageVO.setIsFirstPage(pageNum==1?true:false);// 是否第一页
        pageVO.setIsLastPage(pageNum==pageVO.getLastPage()?true:false);//是否最后一页
        pageVO.setHasPreviousPage(pageNum>1?true:false); //是否有上一页
        pageVO.setHasNextPage(pageNum<pageVO.getPages()?true:false);//是否有下一页

        pageListVO.setPageVO(pageVO);
        pageListVO.setList(sortedList);
        return pageListVO;
    }

}
