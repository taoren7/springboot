package com.morn.common.mongo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MornCommonStarterMongoApplication {

    public static void main(String[] args) {
        SpringApplication.run(MornCommonStarterMongoApplication.class, args);
    }

}
