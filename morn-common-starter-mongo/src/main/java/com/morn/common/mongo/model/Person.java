package com.morn.common.mongo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author: 陶韧
 * @Date: Created in 2019/8/6 23:31
 * @Description:
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Person {

    private Integer id;
    private String name;
    private Integer age;
}
