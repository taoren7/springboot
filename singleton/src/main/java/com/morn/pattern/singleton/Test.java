package com.morn.pattern.singleton;

public class Test {

    public static void main(String[] args) {
        int i = 0b100;
        int j = 0100;
        int k = 100;
        int l = 0x100;
        System.out.println("i="+i);
        System.out.println("j="+j);
        System.out.println("k="+k);
        System.out.println("l="+l);
    }
}
