package com.morn.pattern.singleton.service;

/**
 * @Author: 陶韧
 * @Date: Created in 2019/9/27 21:36
 * @Description:
 */
public class Hungry {

    private Hungry() {
    }

    private static final Hungry hungry = new Hungry();

    public static final Hungry getInstance() {
        System.out.println(System.currentTimeMillis()+":"+hungry);
        return hungry;
    }

    public static void main(String[] args) {
        System.out.println(1);
        test();
        System.out.println(2);
        test2();
        System.out.println(3);
    }

    public static void test(){
        int i=1;
        i = 1+2;
        i = 2+1;
    }

    public static void test2(){
        int i=1;
        i = 1+2;
        i = 2+1;
    }
}
