package com.morn.pattern.singleton.service;

import java.io.Serializable;

/**
 * @Author: 陶韧
 * @Date: Created in 2019/9/29 16:44
 * @Description:
 */
public class Singleton implements Serializable {

    private Singleton() {
        synchronized (Singleton.class) {
            if (!InitHolder.initialized) {

                InitHolder.initialized = !InitHolder.initialized;


            } else {
                throw new RuntimeException("单例已被侵犯");
            }
        }
    }

    private static final class SingletonHolder {
        private static final Singleton LAZY = new Singleton();
    }

    private static final class InitHolder {

        private static boolean initialized = false;
    }

    public static final Singleton getInstance() {
        return SingletonHolder.LAZY;
    }

    private Object readResolve() {
        return SingletonHolder.LAZY;
    }



}
