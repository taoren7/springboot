package com.morn.pattern.singleton.service;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

/**
 * @Author: 陶韧
 * @Date: Created in 2019/9/29 17:48
 * @Description:
 */
public class SingletonTest {

    public static void main(String[] args) {
        try {
            Class<Singleton> clz = Singleton.class;
            Constructor<Singleton> c = clz.getDeclaredConstructor();
            c.setAccessible(true);
            Singleton singleton = c.newInstance();
            Field field = clz.getDeclaredField("initialized");
            field.setAccessible(true);
            field.set(null, true);
            Class<?>[] declaredClasses = clz.getDeclaredClasses();
            for (Class<?> declaredClass : declaredClasses) {
                Field declaredField = declaredClass.getDeclaredField("initialized");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
