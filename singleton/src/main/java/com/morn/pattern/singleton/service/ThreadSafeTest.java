package com.morn.pattern.singleton.service;

import java.util.concurrent.CountDownLatch;

/**
 * @Author: 陶韧
 * @Date: Created in 2019/9/27 21:49
 * @Description:
 */
public class ThreadSafeTest {

    public static void main(String[] args) {
        int count = 100;
        CountDownLatch latch = new CountDownLatch(count);
        for (int i = 0; i < count; i++) {
            new Thread() {
                @Override
                public void run() {
                    Hungry.getInstance();
                    latch.countDown();
                }
            }.start();

        }

        try {
            latch.await();
            System.out.println("test end");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
