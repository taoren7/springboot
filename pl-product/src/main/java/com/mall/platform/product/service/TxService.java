package com.mall.platform.product.service;

import com.mall.platform.product.bean.Product;

import java.util.List;

/**
 * @Author: 陶韧
 * @Date: Created in 2019/6/14 23:35
 * @Description:
 */
public interface TxService {

    void batchHandle(List<Product> products);
}
