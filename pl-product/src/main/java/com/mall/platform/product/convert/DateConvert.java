package com.mall.platform.product.convert;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class DateConvert implements Converter<String, Date> {

	@Override
	public Date convert(String source) {
		if (StringUtils.isEmpty(source)) {
			return null;
		}
		try {
			return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(source);
		} catch (ParseException e) {
			try {
				return new SimpleDateFormat("yyyy-MM-dd").parse(source);
			} catch (ParseException e1) {
				throw new IllegalArgumentException("invalid date, source=" + source);
			}
		}
	}

	public static void main(String[] args) throws Exception{
//		String source ="2019-06-14 12:00:00";
//		Date ret = new SimpleDateFormat("yyyy-MM-dd").parse(source);
//		System.out.println(ret);

		String source ="2019-06-14";
		Date ret = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(source);
		System.out.println(ret);
	}
}
