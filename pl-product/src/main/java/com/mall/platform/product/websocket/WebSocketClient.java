package com.mall.platform.product.websocket;

import java.io.IOException;
import java.net.URI;

import javax.websocket.ClientEndpoint;
import javax.websocket.ContainerProvider;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.WebSocketContainer;

@ClientEndpoint
public class WebSocketClient {

	private static Session session = null;
	
	private int count = 0;
	
	@OnOpen
	public void onOpen(Session session){
		this.session = session;
		sendMessage("onOpen hello benny onOpen");
//		try {
//			session.getBasicRemote().sendText("onOpen hello benny onOpen");
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
	}
	
	@OnClose
	public void onClose(){

	}
	
	@OnMessage
	public void onMessage(String message){
		System.out.println("server message:"+message);
		if(count <10){
			sendMessage("onMessage hello benny "+(++count));
		}
	}
	
	@OnError
	public void onError(Throwable thr){

	}

	public WebSocketClient() {
		super();
	}
	
	public WebSocketClient(URI endpointURI) {
		super();
		try {
			WebSocketContainer container = ContainerProvider.getWebSocketContainer();	// 获得WebSocketContainer
			container.connectToServer(WebSocketClient.class, endpointURI);// 该方法会阻塞
			//this.session = session;
			System.out.println(123);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public Session getSession() {
		return session;
	}

	public void setSession(Session session) {
		this.session = session;
	}
	
	
	public void sendMessage(String message){
		try {
			this.session.getBasicRemote().sendText(message);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			try {
				this.session.getBasicRemote().flushBatch();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void close() {
		try {
			this.session.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}