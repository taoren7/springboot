package com.mall.platform.product.service.impl;

import com.mall.platform.product.bean.Product;
import com.mall.platform.product.mapper.ProductMapper;
import com.mall.platform.product.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Author: 陶韧
 * @Date: Created in 2019/6/14 23:01
 * @Description:
 */
@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductMapper mapper;

    @Override
    public Integer add(Product product) {
        return mapper.add(product);
    }

    @Override
    public Integer deleteById(Integer id) {
        int i = 1 + 2;

        return mapper.deleteById(id);
    }

    @Override
    public Integer update(Product product) {
        return mapper.update(product);
    }

    @Override
    public Product getById(Integer id) {
        return mapper.getById(id);
    }

    @Override
    public List<Product> queryList() {
        return mapper.queryList();
    }

    @Override
    @Transactional
    public void batchAdd(List<Product> products) {
        for (Product product : products) {
            this.add(product);
            int i = 1 / 0;
        }
    }

    @Override
    public void batchHandle(List<Product> products) {
        batchAdd(products);
    }


}
