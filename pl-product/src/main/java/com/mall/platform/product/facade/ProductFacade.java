package com.mall.platform.product.facade;

import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.common.util.alisms.client.AlismsClient;
import com.mall.common.ipdb.config.IpdbClient;
import com.mall.platform.product.bean.Product;
import com.mall.platform.product.service.ProductService;
import com.mall.platform.product.service.TxService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: 陶韧
 * @Date: Created in 2019/6/14 23:04
 * @Description:
 */
@Slf4j
@Component
public class ProductFacade {

    @Value("${common.alisms.templateCode}")
    private String templateCode;

    @Autowired
    private ProductService service;

    @Autowired
    private TxService txService;

    //@Autowired
    //private IpdbClient ipdbClient;

    @Autowired
    private AlismsClient alismsClient;

    public Integer add(Product product) {
        return service.add(product);
    }

    public Integer deleteById(Integer id) {
        return service.deleteById(id);
    }

    public Integer update(Product product) {
        return service.update(product);
    }

    public Product getById(Integer id) {
        return service.getById(id);
    }

    public List<Product> queryList() {
        return service.queryList();
    }


    public void batchAdd(List<Product> products) {
        service.deleteById(77);
        txService.batchHandle(products);
    }

//    public List<String> getDistrictByRequestIp(String ip) {
//        String[] arr = ipdbClient.findByIpDistrict(ip);
//        if(arr!=null) {
//            return Arrays.asList(arr);
//        } else {
//            return null;
//        }
//    }

    public SendSmsResponse sendSms(String phone) {
        String signName = "玩呗娱乐";
        Map<String, String> paramMap = new HashMap<>();
        String code = RandomStringUtils.randomNumeric(6);  // 生成验证码
        log.info("code:{}", code);
        paramMap.put("code", code);
        SendSmsResponse sendSmsResponse = alismsClient.sendSms(phone, signName, templateCode, paramMap);
        return sendSmsResponse;
    }
}
