package com.mall.platform.product.model;

import java.io.Serializable;

/**
 * @Author: 陶韧
 * @Date: Created in 2019/6/14 21:44
 * @Description:
 */
public class RestResponse<T> implements Serializable {
    private int code;
    private String message;
    private T data;

    public RestResponse() {
        this(null);
    }

    public RestResponse(T data) {
        this(200,"SUCCESS",data);
    }

    public RestResponse(int code, String message) {
        this(code,message,null);
    }

    public RestResponse(int code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public boolean isSuccess() {
        return code==200;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "RestResponse{" +
                "code=" + code +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}
