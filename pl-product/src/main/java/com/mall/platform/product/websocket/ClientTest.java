package com.mall.platform.product.websocket;

import java.io.IOException;
import java.net.URI;

/**
 * @Author: 陶韧
 * @Date: Created in 2019/7/25 17:11
 * @Description:
 */
public class ClientTest {

    public static void main(String[] args) {
        try {
            String uri = "ws://localhost:8080/websocket/hhh";
            System.out.println("Connecting to " + uri);
            WebSocketClient webSocketClient = new WebSocketClient(URI.create(uri));
            webSocketClient.sendMessage("test");
            java.io.BufferedReader r=new java.io.BufferedReader(new java.io.InputStreamReader(System.in));
            while(true){
                String line=r.readLine();
                if(line.equals("quit")) {
                    webSocketClient.close();
                    break;
                }
                webSocketClient.sendMessage(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
