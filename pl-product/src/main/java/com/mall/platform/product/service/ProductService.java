package com.mall.platform.product.service;

import com.mall.platform.product.bean.Product;

import java.util.List;

/**
 * @Author: 陶韧
 * @Date: Created in 2019/6/14 23:00
 * @Description:
 */
public interface ProductService {

    Integer add(Product product);   

    Integer deleteById(Integer id);

    Integer update(Product product);

    Product getById(Integer id);

    List<Product> queryList();

    void batchAdd(List<Product> products);

    void batchHandle(List<Product> products);

}
