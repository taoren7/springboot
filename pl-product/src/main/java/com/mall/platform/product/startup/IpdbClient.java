//package com.mall.platform.product.startup;
//
//import lombok.extern.slf4j.Slf4j;
//import net.ipip.ipdb.City;
//import net.ipip.ipdb.District;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.boot.ApplicationArguments;
//import org.springframework.boot.ApplicationRunner;
//import org.springframework.stereotype.Component;
//
//import java.io.IOException;
//
///**
// * 最新的ip库及相关配置，初始化
// */
//@Slf4j
//@Component
//public class IpdbClient implements ApplicationRunner {
//
//	@Value("${ipdb.path.district}")
//	private String ipQuxian;
//
//	@Value("${ipdb.path.city}")
//	private String ip;
//
//	private City city;
//
//	private District district;
//
//	private final String LANGUAGE = "CN";
//
//	@Override
//	public void run(ApplicationArguments args) throws Exception {
//		init();
//	}
//
//	public void init(){
//		try {
//			city = new City(ip);
//		} catch (IOException e) {
//			log.error("ip city init error", e);
//		}
//		try {
//			district = new District(ipQuxian);
//		} catch (IOException e) {
//			log.error("ip district init error", e);
//		}
//	}
//
//	/**
//	 * 根据ip地级库及ip查找地级市区域
//	 * @param ip
//	 * @return
//	 */
//	public String[] findByIpCity(String ip) {
//		if (city != null) {
//			try {
//				return city.find(ip, LANGUAGE);
//			} catch (Exception e) {
//				log.error("ip city find error", e);
//			}
//		}
//		return null;
//	}
//
//	/**
//	 * 根据ip县级库及ip查找县级市县区域
//	 * @param ip
//	 * @return
//	 */
//	public String[] findByIpDistrict(String ip) {
//		if (district != null) {
//			try {
//				return district.find(ip, LANGUAGE);
//			} catch (Exception e) {
//				log.error("ip district find error", e);
//			}
//		}
//		return null;
//	}
//
//}
