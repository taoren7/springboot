package com.mall.platform.product.service.impl;

import com.mall.platform.product.bean.Product;
import com.mall.platform.product.service.ProductService;
import com.mall.platform.product.service.TxService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: 陶韧
 * @Date: Created in 2019/6/14 23:36
 * @Description:
 */
@Service
public class TxServiceImpl implements TxService {

    @Autowired
    private ProductService productService;

    @Override
    public void batchHandle(List<Product> products) {
        productService.batchHandle(products);
    }
}
