package com.mall.platform.product.config;


import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.transaction.annotation.EnableTransactionManagement;

//@EnableIpdb
@MapperScan("com.mall.platform.product.mapper")
@SpringBootApplication(scanBasePackages = "com.mall.platform.product")
@EnableTransactionManagement
public class PlProductApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(PlProductApplication.class, args);
//        AlismsProperties bean = context.getBean(AlismsProperties.class);
//        System.out.println(bean);

//        ProductMapper mapper = context.getBean(ProductMapper.class);

//        Product product = new Product();
//        product.setPname("MP4");
//        product.setType("数码产品");
//        product.setPrice(200);
//        product.setCreateTime(new Date());
////        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
////        product.setCreateTime(timestamp);
//        mapper.add(product);


//        Integer ret = mapper.deleteById(4);
//        System.out.println(ret);


//        Product product = new Product();
//        product.setPid(5);
//        product.setPname("MP5");
//        product.setType("数码产品");
//        product.setPrice(500);
//        mapper.update(product);

//        Product p = mapper.getById(5);
//        System.out.println(p);
//        System.out.println(p.getCreateTime());
//        List<Product> products = mapper.queryList();
//        System.out.println(products);
    }

}
