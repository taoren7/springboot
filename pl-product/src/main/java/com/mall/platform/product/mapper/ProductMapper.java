package com.mall.platform.product.mapper;

import com.mall.platform.product.bean.Product;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * @Author: 陶韧
 * @Date: Created in 2019/6/14 17:58
 * @Description:
 */
//@Mapper
public interface ProductMapper {

    //@Insert("insert into product(pname,type,price,create_time) values(#{pname},#{type},#{price},#{createTime})")
    Integer add(Product product);

    //@Delete("delete from product where pid=#{id}")
    Integer deleteById(Integer id);

    //@Update("update product set pname=#{pname},type=#{type},price=#{price} where pid=#{pid}")
    Integer update(Product product);

    //@Select("select * from product where pid=#{arg2}")
    Product getById(Integer id);

    //@Select("select * from product order by pid desc")
    List<Product> queryList();
}
