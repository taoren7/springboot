package com.mall.platform.product.bean;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author: 陶韧
 * @Date: Created in 2019/6/14 17:56
 * @Description:
 */
@Data
public class Product implements Serializable {

    private Integer pid;
    private String pname;
    private String type;
    private Integer price;
    //    private Timestamp createTime;
    //    @JsonFormat(pattern="yyyy-MM-dd")
    private Date createTime;

}
