package com.mall.platform.product;

import com.mall.platform.product.config.PlProductApplication;
import org.springframework.util.StringUtils;

public class ProductApplication {

    public static void main(String[] args) {
        String tmp = System.getProperty("spring.profiles.active");
        if (StringUtils.isEmpty(tmp)) {
            System.setProperty("spring.profiles.active", "dev");
        }
        PlProductApplication.main(args);

    }

}
