package com.mall.platform.product.web;

import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.common.util.alisms.client.AlismsClient;
import com.mall.platform.product.bean.Product;
import com.mall.platform.product.facade.ProductFacade;
import com.mall.platform.product.mapper.ProductMapper;
import com.mall.platform.product.model.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @描述 
 * @创建人 陶韧
 * @创建时间 2019/10/26 12:06 
 * @修改人和其它信息 
 */
@RestController
@RequestMapping("/product")
public class ProductRest {

    @Autowired
    private ProductFacade facade;

    @PostMapping("/add")
    public RestResponse<Integer> add(@RequestBody Product p) {
        Integer ret = facade.add(p);
        return new RestResponse<>(ret);
    }

    @PostMapping("/batchAdd")
    //@Transactional
    /**
     * @描述 
     * @参数 [products]
     * @返回值 com.mall.platform.product.model.RestResponse<java.lang.Void>
     * @创建人 陶韧
     * @创建时间 2019/10/26 12:09
     * @修改人和其它信息 
     */
    public RestResponse<Void> batchAdd(@RequestBody List<Product> products) {
        facade.batchAdd(products);
        return new RestResponse<>();
    }

    @PostMapping("/deleteById")
    public RestResponse deleteById(Integer id) {
        facade.deleteById(id);
        return new RestResponse();
    }

    
    @PostMapping("/update")
    
    public RestResponse<Integer> update(Product p) {
        Integer ret = facade.update(p);
        return new RestResponse<>(ret);
    }

    
    @GetMapping("/getById")
    public RestResponse<Product> getById(HttpServletRequest request, Integer id) {
        
        String requestURI = request.getRequestURI();
        StringBuffer requestURL = request.getRequestURL();
        String remoteAddr = request.getRemoteAddr();
        String remoteHost = request.getRemoteHost();
        int remotePort = request.getRemotePort();
        String remoteUser = request.getRemoteUser();
        String queryString = request.getQueryString();
        Product p = facade.getById(id);
        return new RestResponse<>(p);
    }

    @GetMapping("/queryList")
    public RestResponse<List<Product>> queryList() {
        List<Product> list = facade.queryList();
        return new RestResponse<>(list);
    }

//    @GetMapping("/getDistrictByRequestIp")
//    public RestResponse<List<String>> getDistrictByRequestIp(HttpServletRequest request) {
//        String remoteAddr = request.getRemoteAddr();
//        String remoteHost = request.getRemoteHost();
//        String ip = "1.30.237.11";
//        List<String> list = facade.getDistrictByRequestIp(ip);
//        return new RestResponse(list);
//    }

    @PostMapping("sendSms")
    public RestResponse<SendSmsResponse> sendSms(String phone) {
        SendSmsResponse sendSmsResponse = facade.sendSms(phone);
        return new RestResponse(sendSmsResponse);
    }

    @GetMapping("/book")
    public RestResponse<String> book(Integer id) {
        String ret = "product.book."+id;
        return new RestResponse<>(ret);
    }

}
