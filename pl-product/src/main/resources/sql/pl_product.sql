create database pl_product default charset utf8;

DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `pid` int(11) NOT NULL AUTO_INCREMENT COMMENT '商品ID',
  `pname` varchar(255) NOT NULL COMMENT '商品名称',
  `type` varchar(255) NOT NULL COMMENT '商品类型',
  `price` int(11) NOT NULL COMMENT '商品价格',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品表';


