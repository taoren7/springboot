package com.mall.platform.product;

import com.mall.platform.product.config.PlProductApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = PlProductApplication.class, webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RestTest {

    @Autowired
    private TestRestTemplate rt;


    @Test
    public void test() {

        String response = rt.getForObject("/product/book?id={id}", String.class, 1);
        System.out.println(response);
    }
}
