package com.mall.platform.product;

import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.common.util.alisms.client.AlismsClient;
import com.mall.common.ipdb.config.IpdbClient;
import com.mall.platform.product.config.PlProductApplication;
import com.mall.platform.product.facade.ProductFacade;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = PlProductApplication.class)
//@SpringBootTest(classes = ProductApplication.class)
public class PlProductApplicationTests {

//    @Autowired
//    private IpdbClient ipdbClient;
    @Autowired
    private ProductFacade facade;

    @Test
    public void Loads() {
    }

    @Test
    public void sendSms() {
        SendSmsResponse sendSmsResponse = facade.sendSms("13923356319");
        System.out.println(sendSmsResponse);

    }

//    @Test
//    public void testIpdb() {
//        String districtIp = "1.30.237.11";
//        String cityIp = "118.28.1.1";
//        String ip = cityIp;
//        String[] districtArr = ipdbClient.findByIpDistrict(ip);
//        System.out.println(Arrays.asList(districtArr));
//        String[] cityArr = ipdbClient.findByIpCity(ip);
//        System.out.println(Arrays.asList(cityArr));
//    }

}
