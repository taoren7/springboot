package com.mall.platform.product;

import com.mall.platform.product.config.PlProductApplication;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.Environment;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(properties={"app.name=springboottest", "spring.profiles.active=dev"},
        classes = {PlProductApplication.class})
public class EnvironmentTest {

    @Autowired
    private Environment env;
    @Autowired
    private ConfigurableEnvironment configEnv;


    @Before
    public void init() {
        //configEnv.setActiveProfiles("test");

    }

    @Test
    public void test() {
        Assert.assertEquals("springboottest", env.getProperty("app.name"));
    }
}
