package com.mall.platform.product;

import com.mall.platform.product.config.PlProductApplication;
import com.mall.platform.product.config.TestBeanConfiguration;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {PlProductApplication.class, TestBeanConfiguration.class})
public class ApplicationContextTest {

    @Autowired
    private ApplicationContext context;

    @Test
    public void test() {
        Runnable bean = context.getBean(Runnable.class);
        Assert.assertNotNull(bean);
        //Assert.assertEquals("abc", "abc");
    }
}
