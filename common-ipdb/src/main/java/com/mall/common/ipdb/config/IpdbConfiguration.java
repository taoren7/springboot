package com.mall.common.ipdb.config;

import lombok.extern.slf4j.Slf4j;
import net.ipip.ipdb.City;
import net.ipip.ipdb.District;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

/**
 * @Author: 陶韧
 * @Date: Created in 2019/6/15 10:15
 * @Description:
 */
@Configuration
@Slf4j
@EnableConfigurationProperties(IpdbProperties.class)
public class IpdbConfiguration {

    @Autowired
    private IpdbProperties ipdbProperties;

    @Bean
    //public City city(IpdbProperties ipdbProperties) {
    public City city() {
        City city = null;
        try {
            city = new City(ipdbProperties.getCity());
        } catch (IOException e) {
            log.error("ip city init error", e);
        }
        return city;
    }

    @Bean
    //public District district(IpdbProperties ipdbProperties) {
    public District district() {
        District district = null;
        try {
            district = new District(ipdbProperties.getDistrict());
        } catch (IOException e) {
            log.error("ip district init error", e);
        }
        return district;
    }

    @Bean
    public IpdbClient ipdbClient(City city, District district) {
        return new IpdbClient(city, district);
    }
}
