package com.mall.common.ipdb.config;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @Author: 陶韧
 * @Date: Created in 2019/6/15 11:11
 * @Description:
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import(IpdbConfiguration.class)
public @interface EnableIpdb {
}
