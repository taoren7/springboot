package com.mall.common.ipdb.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @Author: 陶韧
 * @Date: Created in 2019/6/15 10:12
 * @Description:
 */
@Data
@ConfigurationProperties(prefix = "ipdb.path")
public class IpdbProperties {

    private String city;
    private String district;
}
