package com.mall.common.ipdb.config;

import lombok.extern.slf4j.Slf4j;
import net.ipip.ipdb.City;
import net.ipip.ipdb.District;

/**
 * @Author: 陶韧
 * @Date: Created in 2019/6/15 10:22
 * @Description:
 */
@Slf4j
public class IpdbClient {

    private City city;
    private District district;
    private final String LANGUAGE = "CN";

    public IpdbClient(City city, District district) {
        this.city = city;
        this.district = district;
    }

    /**
     * 根据ip地级库及ip查找地级市区域
     * @param ip
     * @return
     */
    public String[] findByIpCity(String ip) {
        if (city != null) {
            try {
                return city.find(ip, LANGUAGE);
            } catch (Exception e) {
                log.error("ip city find error", e);
            }
        }
        return null;
    }

    /**
     * 根据ip县级库及ip查找县级市县区域
     * @param ip
     * @return
     */
    public String[] findByIpDistrict(String ip) {
        if (district != null) {
            try {
                return district.find(ip, LANGUAGE);
            } catch (Exception e) {
                log.error("ip district find error", e);
            }
        }
        return null;
    }

}
