package com.common.util.alisms.config;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;
import com.common.util.alisms.client.AlismsClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author: 陶韧
 * @Date: Created in 2019/6/15 18:30
 * @Description:
 */
@Slf4j
@Configuration
@EnableConfigurationProperties(AlismsProperties.class)
public class AlismsConfiguration {

    @Bean
    public IAcsClient acsClient(AlismsProperties alismsProperties) {
        log.info("初始化阿里短信acsClient");
        // 可自助调整超时时间
        System.setProperty("sun.net.client.defaultConnectTimeout", alismsProperties.getDefaultConnectTimeout());
        System.setProperty("sun.net.client.defaultReadTimeout", alismsProperties.getDefaultReadTimeout());
        // 初始化acsClient,暂不支持region化
        try {
            IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", alismsProperties.getAccessKeyId(), alismsProperties.getAccessKeySecret());
            DefaultProfile.addEndpoint("cn-hangzhou", "cn-hangzhou", "Dysmsapi", alismsProperties.getDomain());
            return new DefaultAcsClient(profile);
        } catch (ClientException e) {
            log.info("初始化阿里短信acsClient异常", e);
        }
        return null;
    }

    @Bean
    public AlismsClient alismsClient(IAcsClient acsClient) {
        return new AlismsClient(acsClient);
    }

}
