package com.common.util.alisms.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @Author: 陶韧
 * @Date: Created in 2019/6/15 18:30
 * @Description:
 */
@Data
@ConfigurationProperties("common.alisms")
public class AlismsProperties {
    /**api域名*/
    private String domain;

    /**accessKey*/
    private String accessKeyId;

    /**accessKeySecret */
    private String accessKeySecret;

    /**连接超时时间 */
    private String defaultConnectTimeout;

    /**读取超时时间 */
    private String defaultReadTimeout;

    /**模板编码 */
    //private String templateCode;
}
