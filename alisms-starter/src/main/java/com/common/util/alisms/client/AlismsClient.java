package com.common.util.alisms.client;

import com.alibaba.fastjson.JSON;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.exceptions.ClientException;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * @Author: 陶韧
 * @Date: Created in 2019/6/15 18:50
 * @Description:
 */
@Slf4j
public class AlismsClient {

    private IAcsClient acsClient;

    public AlismsClient(IAcsClient acsClient) {
        this.acsClient = acsClient;
    }

    /**
     * String code=SendSmsResponse.getCode()
     * String message=SendSmsResponse.getMessage()
     * code:
     *      "OK"//发送成功
     *          message: "OK"
     *      "isv.BUSINESS_LIMIT_CONTROL"//发送次数超出限制
     *          message:
     *              message.startsWith("触发天级")//短信发送超出每天限制//触发天级流控Permits:10
     *              message.startsWith("触发小时级")//短信发送超出每小时限制//触发小时级流控Permits:5
     *              message.startsWith("触发分钟级")//短信发送超出每分钟限制//触发分钟级流控Permits:1
     *              其它message//短信发送操作频繁
     *      "isv.MOBILE_NUMBER_ILLEGAL" //手机号码格式有误
     */
//    public void sendSms(String phone, String signName, String templateCode, String content) {
    public SendSmsResponse sendSms(String phone, String signName, String templateCode, Map<String, String> paramMap) {
        if(acsClient==null) {
            log.error("发送短信异常，acsClient is null");
            return null;
        }
        // 组装请求对象-具体描述见控制台-文档部分内容
        SendSmsRequest request = new SendSmsRequest();
        // 必填:待发送手机号
        request.setPhoneNumbers(phone);
        // 必填:短信签名-可在短信控制台中找到
        // signName="玩呗娱乐"
        request.setSignName(signName);
        // 必填:短信模板-可在短信控制台中找到
        request.setTemplateCode(templateCode);
        //"【玩呗娱乐】你的验证码为：" + ${code} + "。请不要把验证码泄露给其他人！10分钟内有效。"
        // 可选:模板中的变量替换JSON串,如模板内容为"您的验证码为${code}"时,此处的值为
//        request.setTemplateParam("{\"code\":\"" + content + "\"}");
        request.setTemplateParam(JSON.toJSONString(paramMap));
        // 选填-上行短信扩展码(无特殊需求用户请忽略此字段)
        // request.setSmsUpExtendCode("90997");
        // 可选:outId为提供给业务方扩展字段,最终在短信回执消息中将此值带回给调用者
        // request.setOutId("yourOutId");
        try {
            SendSmsResponse response = acsClient.getAcsResponse(request);
            return response;
        } catch (ClientException e) {
            log.error("发送短信失败", e);
        }
        return null;
    }
}
